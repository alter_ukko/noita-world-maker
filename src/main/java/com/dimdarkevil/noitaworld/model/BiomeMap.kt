package com.dimdarkevil.noitaworld.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.awt.image.BufferedImage

data class BiomeMap(
	val width: Int = 0,
	val height: Int = 0,
	val startY: Int = 0,
	@JsonIgnore
	var bi: BufferedImage? = null,
)
