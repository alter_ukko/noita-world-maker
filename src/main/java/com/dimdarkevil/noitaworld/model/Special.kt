package com.dimdarkevil.noitaworld.model

import java.awt.image.BufferedImage
import kotlin.reflect.KClass

data class Special(
	var name: String = "",
	var desc: String = "",
	var image_filename: String = "",
	var cls: KClass<out SceneObjConfig>,
	var sprite_offset_x: Int = 0,
	var sprite_offset_y: Int = 0,
	var image: BufferedImage? = null,
)
