package com.dimdarkevil.noitaworld.model

import com.dimdarkevil.noitaworld.HOME
import java.io.File

data class AppConfig(
	var winX: Int = -1,
	var winY: Int = -1,
	var winWidth: Int = -1,
	var winHeight: Int = -1,
	var splitterPosition: Int = -1,
	var sideSplitterPosition: Int = -1,

	var sceneWinX: Int = -1,
	var sceneWinY: Int = -1,
	var sceneWinWidth: Int = -1,
	var sceneWinHeight: Int = -1,
	var sceneSplitterPosition: Int = -1,
	var sceneSideSplitterPosition: Int = -1,

	var noitaSaveFolder : String = File(HOME, "AppData/LocalLow/Nolla_Games_Noita").path,
	var noitaExeFile : String = File("C:/Program Files (x86)/Steam/steamapps/common/Noita/noita.exe").let {
		if (it.exists()) it.path else ""
	},
	var biomeTableColumnWidths: List<Int> = listOf(),
	var sceneTableColumnWidths: List<Int> = listOf(),
	var materialTableColumnWidths: List<Int> = listOf(),
	var entityTableColumnWidths: List<Int> = listOf(),
	var objectTableColumnWidths: List<Int> = listOf(),
	var perkTableColumnWidths: List<Int> = listOf(),
	var spellTableColumnWidths: List<Int> = listOf(),
	var specialTableColumnWidths: List<Int> = listOf(),
)
