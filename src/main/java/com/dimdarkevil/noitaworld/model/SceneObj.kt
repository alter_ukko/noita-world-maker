package com.dimdarkevil.noitaworld.model

data class SceneObj(
	var id: Int = 0,
	var type: SceneObjType = SceneObjType.ENTITY,
	var resource_name: String = "",
	var short_name: String = "",
	var x: Int = 0,
	var y: Int = 0,
	var w: Int = 0,
	var h: Int = 0,
	var x_offset: Int = 0,
	var y_offset: Int = 0,
	var config: SceneObjConfig? = null,
)
