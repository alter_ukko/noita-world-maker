package com.dimdarkevil.noitaworld.model

import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
sealed class SceneObjConfig

data class PotionConfig(
	var material: String = "",
) : SceneObjConfig()

data class ChestConfig(
	var type: SceneObjType = SceneObjType.ENTITY,
	var resource_name: String = "",
) : SceneObjConfig()

data class TeleportConfig(
	var remote_x: Int = 0,
	var remote_y: Int = 0,
) : SceneObjConfig()