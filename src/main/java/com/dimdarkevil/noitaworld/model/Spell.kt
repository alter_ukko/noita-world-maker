package com.dimdarkevil.noitaworld.model

import java.awt.image.BufferedImage

data class Spell(
	var id: String = "",
	var name: String = "",
	var description: String = "",
	var sprite: String = "",
	var english_name: String = "",
	var english_desc: String = "",
	var image: BufferedImage? = null,
)
