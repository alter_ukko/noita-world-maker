package com.dimdarkevil.noitaworld.model

sealed interface Change {}

data class Undo(
	val changes: MutableList<Change> = mutableListOf()
)

data class PixelChange(
	val x: Int,
	val y: Int,
	val fromColor: Int,
	val toColor: Int,
) : Change

data class PlayerStartChange(
	val fromX: Int,
	val fromY: Int,
	val toX: Int,
	val toY: Int,
) : Change

data class SceneChange(
	val scene: Scene,
	val fromX: Int,
	val fromY: Int,
	val toX: Int,
	val toY: Int,
) : Change

data class ObjectChange(
	val obj: SceneObj,
	val fromX: Int,
	val fromY: Int,
	val toX: Int,
	val toY: Int,
) : Change
