package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.Entity
import java.awt.image.BufferedImage
import javax.swing.table.AbstractTableModel

class EntityTableModel : AbstractTableModel() {
	private var items = listOf<Entity>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 3
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.image
			1 -> b.short_filename
			2 -> b.english_name.ifEmpty { b.name }
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "img"
			1 -> "file"
			2 -> "name"
			else -> "[unknown]"
		}
	}

	override fun getColumnClass(columnIndex: Int): Class<*> {
		return when (columnIndex) {
			0 -> BufferedImage::class.java
			else -> String::class.java
		}
	}

	fun setEntities(lst: List<Entity>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun entityAt(row: Int) : Entity? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}

}