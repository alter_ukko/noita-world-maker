package com.dimdarkevil.noitaworld.tablemodel

import com.dimdarkevil.noitaworld.model.Material
import javax.swing.table.AbstractTableModel

class MaterialTableModel : AbstractTableModel() {
	private var items = listOf<Material>()

	override fun getRowCount() : Int {
		return items.size
	}

	override fun getColumnCount() : Int {
		return 3
	}

	override fun getValueAt(rowIndex: Int, columnIndex: Int): Any? {
		val b = items[rowIndex]
		return when (columnIndex) {
			0 -> b.rgb
			1 -> b.name
			2 -> b.english_name
			else -> "[unknown]"
		}
	}

	override fun getColumnName(column: Int): String {
		return when (column) {
			0 -> "color"
			1 -> "name"
			2 -> "label"
			else -> "[unknown]"
		}
	}

	override fun getColumnClass(columnIndex: Int): Class<*> {
		return when (columnIndex) {
			0 -> Int::class.java
			else -> String::class.java
		}
	}

	fun setMaterials(lst: List<Material>) {
		items = lst
		this.fireTableDataChanged()
	}

	fun getMaterialAt(row: Int) : Material? {
		if (row < 0 || row >= items.size) return null
		return items[row]
	}
}