package com.dimdarkevil.noitaworld

data class RgbClr(
	var r: Float,
	var g: Float,
	var b: Float
)

data class HsvClr(
	var h: Float,
	var s: Float,
	var v: Float
)

data class RgbIntClr(
	var r: Int,
	var g: Int,
	var b: Int
)

fun rgb2hsv(cin: RgbClr, cout: HsvClr) {
	var min = if (cin.r < cin.g) cin.r else cin.g
	min = if (min < cin.b) min else cin.b

	var max = if (cin.r > cin.g) cin.r else cin.g
	max = if (max > cin.b) max else cin.b

	cout.v = max
	val delta = max-min
	if (delta < 0.00001f) {
		cout.s = 0.0f
		cout.h = 0.0f
		return
	}
	if (max > 0.0f) {
		cout.s = (delta / max)
	} else {
		cout.s = 0.0f
		cout.h = 0.0f
		return
	}
	if (cin.r >= max) {
		cout.h = ( cin.g - cin.b ) / delta
	} else if (cin.g >= max) {
		cout.h = 2.0f + ( cin.b - cin.r ) / delta
	} else if (cin.b >= max) {
		cout.h = 4.0f + ( cin.r - cin.g ) / delta
	}
	cout.h *= 60.0f
	if (cout.h < 0.0f) {
		cout.h += 360.0f
	}
}

fun hsv2rgb(cin: HsvClr, cout: RgbClr) {
	if (cin.s <= 0.0f) {
		cout.r = cin.v
		cout.g = cin.v
		cout.b = cin.v
		return
	}
	var hh = cin.h
	if (hh >= 360.0f) hh = 0.0f
	hh /= 60.0f
	val i = hh.toLong()
	val ff = hh - i
	val p = cin.v * (1.0f - cin.s)
	val q = cin.v * (1.0f - cin.s * ff)
	val t = cin.v * (1.0f - cin.s * (1.0f - ff))

	when (i) {
		0L -> {
			cout.r = cin.v
			cout.g = t
			cout.b = p
		}
		1L -> {
			cout.r = q
			cout.g = cin.v
			cout.b = p
		}
		2L -> {
			cout.r = p
			cout.g = cin.v
			cout.b = t
		}
		3L -> {
			cout.r = p
			cout.g = q
			cout.b = cin.v
		}
		4L -> {
			cout.r = t
			cout.g = p
			cout.b = cin.v
		}
		5L -> {
			cout.r = cin.v
			cout.g = p
			cout.b = q
		}
		else -> {
			cout.r = cin.v
			cout.g = p
			cout.b = q
		}
	}
}

fun scaleRgb(rclr: RgbClr) =
	RgbClr(rclr.r * 255.0f, rclr.g * 255.0f, rclr.b * 255.0f)
