package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.model.Vec2
import com.dimdarkevil.noitaworld.model.Vec2i
import java.awt.Font
import kotlin.math.min

/**
 * The way we handle units is that at scale 1.0, screen pixels are the same as world units.
 * So for the viewport, the world-unit width is the pixel width of the window divided by
 * the scale (same for height).
 *
 * Examples:
 * At scale 2.0, a 1000x400-pixel window would be showing 500x200 world units.
 * At scale 0.5, a 1000x400-pixel window would be showing 2000x800 world units.
 */
class Viewport(pixW: Int, pixH: Int, defaultScale: Double = 1.0) {
	private var scale: Double = defaultScale
	private var pixWidth = pixW.toDouble()
	private var pixHeight = pixH.toDouble()
	private val scaledTL = Vec2(0.0, 0.0)
	private val scaledBR = Vec2(pixWidth / scale, pixHeight / scale)
	private var f: Font = Font("Roboto", Font.PLAIN, 28)
	private val pixelPos = Vec2i(0, 0)
	private val worldPos = Vec2(0.0, 0.0)
	private val worldPosInt = Vec2i(0, 0)

	fun getScale() = scale

	fun resize(pixW: Int, pixH: Int) {
		pixWidth = pixW.toDouble()
		pixHeight = pixH.toDouble()
		// resize doesn't change the origin (TR)
		scaledBR.x = scaledTL.x + (pixWidth / scale)
		scaledBR.y = scaledTL.y + (pixHeight / scale)
	}

	fun rescaleAroundCenter(sc: Double) {
		// can't allow zero scale because then you'd see infinity
		if (sc < 0.01) return
		// Get the midpoint, because we'll rescale around the center
		// (maybe change this later to rescale around the mouse cursor?)
		val midX = scaledTL.x + ((scaledBR.x - scaledTL.x) / 2.0)
		val midY = scaledTL.y + ((scaledBR.y - scaledTL.y) / 2.0)
		scale = sc
		// Get the distance of the new viewport origin from the midpoint
		val newHalfW = ((pixWidth / scale) / 2.0)
		val newHalfH = ((pixHeight / scale) / 2.0)
		// set the new viewport bounds
		scaledTL.x = midX - newHalfW
		scaledTL.y = midY - newHalfH
		scaledBR.x = midX + newHalfW
		scaledBR.y = midY + newHalfH
	}

	// xPix,yPix are screen (pixel) offsets from the viewpoint origin
	fun rescaleAroundPoint(xPix: Int, yPix: Int, sc: Double) {
		if (sc < 0.01) return
		// Calculate the world-space offset of the locus point from the viewport origin
		// This will remain at the same pixel location after scaling
		val xLocus = (xPix.toFloat() / scale)
		val yLocus = (yPix.toFloat() / scale)
		// Calculate the percentage across and down of the locus point
		val xpct = xLocus / (scaledBR.x - scaledTL.x)
		val ypct = yLocus / (scaledBR.y - scaledTL.y)
		scale = sc
		// Calculate the offset of the locus point from what will be the new viewport origin
		val worldUnitsLeftOfLocus = (pixWidth / scale) * xpct
		val worldUnitsAboveLocus = (pixHeight / scale) * ypct
		// set the new viewport bounds
		scaledTL.x = (scaledTL.x + xLocus) - worldUnitsLeftOfLocus
		scaledTL.y = (scaledTL.y + yLocus) - worldUnitsAboveLocus
		scaledBR.x = scaledTL.x + (pixWidth / scale)
		scaledBR.y = scaledTL.y + (pixHeight / scale)
	}

	// move by a number of screen pixels
	fun moveByPix(dxPix: Int, dyPix: Int) {
		scaledTL.x = scaledTL.x + (dxPix.toDouble() / scale)
		scaledTL.y = scaledTL.y + (dyPix.toDouble() / scale)
		// clamp to 0,0 origin (don't allow negative)
		//if (scaledTL.x < 0.0) scaledTL.x = 0.0
		//lif (scaledTL.y < 0.0) scaledTL.y = 0.0
		scaledBR.x = scaledTL.x + (pixWidth / scale)
		scaledBR.y = scaledTL.y + (pixHeight / scale)
	}

	// is any point in the rectangle within the viewport
	fun inBounds(x: Double, y: Double, w: Double, h: Double) : Boolean {
		return (pointInBounds(x, y) || pointInBounds(x+w, y) || pointInBounds(x, y+h) || pointInBounds(x+w, y+h))
	}

	// is a point (in world units) within the viewport
	fun pointInBounds(x: Double, y: Double) : Boolean {
		return (x >= scaledTL.x && x <= scaledBR.x && y >= scaledTL.y && y <= scaledBR.y )
	}

	// Given a point in world coordinates, return the point in screen (pixel) coordinates
	fun getPixelPos(wx: Double, wy: Double) : Vec2i {
		//return Vec2i(((wx - scaledTL.x) * scale).toInt(), ((wy - scaledTL.y) * scale).toInt())
		pixelPos.x = ((wx - scaledTL.x) * scale).toInt()
		pixelPos.y = ((wy - scaledTL.y) * scale).toInt()
		return pixelPos
	}

	fun getWorldPos(xPix: Int, yPix: Int) : Vec2 {
		//return Vec2(scaledTL.x + (xPix.toDouble() / scale), scaledTL.y + (yPix.toDouble() / scale))
		worldPos.x = scaledTL.x + (xPix.toDouble() / scale)
		worldPos.y = scaledTL.y + (yPix.toDouble() / scale)
		return worldPos
	}

	fun getWorldPosInt(xPix: Int, yPix: Int) : Vec2i {
		//return Vec2(scaledTL.x + (xPix.toDouble() / scale), scaledTL.y + (yPix.toDouble() / scale))
		worldPosInt.x = (scaledTL.x + (xPix.toDouble() / scale)).toInt()
		worldPosInt.y = (scaledTL.y + (yPix.toDouble() / scale)).toInt()
		return worldPosInt
	}

	fun origin() = scaledTL

	fun print() {
		val viewWorldW = (scaledBR.x - scaledTL.x)
		val viewWorldH = (scaledBR.y - scaledTL.y)
		println("tl: (${scaledTL.x},${scaledTL.y}) br: (${scaledBR.x},${scaledBR.y}) w: ${viewWorldW}, h: ${viewWorldH}")
	}

	fun getTopLeft() = scaledTL

	fun getBottomRight() = scaledBR

	fun fitImage(imgWidth: Int, imgHeight: Int) {
		report()
		val targetWidth = (pixWidth * 0.9)
		val targetHeight = (pixHeight * 0.9)
		val horzScale = targetWidth / imgWidth.toDouble()
		val vertScale = targetHeight / imgHeight.toDouble()
		val newScale = min(horzScale, vertScale)
		scale = newScale
		scaledTL.x = ((pixWidth / scale) - imgWidth.toDouble()) / -2.0
		scaledTL.y = ((pixHeight / scale) - imgHeight.toDouble()) / -2.0
		scaledBR.x = scaledTL.x + (pixWidth / scale)
		scaledBR.y = scaledTL.y + (pixHeight / scale)
		report()
	}

	fun report() {
		println("pixWidth: ${pixWidth}, pixHeight: ${pixHeight}, scale: ${scale}, scaledTL: ${scaledTL}")
	}

}