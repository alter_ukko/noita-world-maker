package com.dimdarkevil.noitaworld.listener

import com.dimdarkevil.noitaworld.model.SceneObj

interface SceneStatusListener {
	fun showStatus(x:Int, y: Int, msg: String)
	fun objectPosChanged(obj: SceneObj)
	fun replacementColorSelected(color: Int)
}