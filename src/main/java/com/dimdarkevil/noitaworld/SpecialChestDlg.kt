package com.dimdarkevil.noitaworld

import com.dimdarkevil.noitaworld.cellrenderer.BiomeTableColorCellRenderer
import com.dimdarkevil.noitaworld.cellrenderer.EntityTableImgCellRenderer
import com.dimdarkevil.noitaworld.io.NoitaData
import com.dimdarkevil.noitaworld.model.*
import com.dimdarkevil.noitaworld.tablemodel.EntityTableModel
import com.dimdarkevil.noitaworld.tablemodel.MaterialTableModel
import com.dimdarkevil.noitaworld.tablemodel.PerkTableModel
import com.dimdarkevil.noitaworld.tablemodel.SpellTableModel
import com.dimdarkevil.swingutil.OkCancelModalDialog
import com.dimdarkevil.swingutil.PaddedPanel
import java.awt.BorderLayout
import java.awt.event.ActionEvent
import java.awt.image.BufferedImage
import javax.swing.*

class SpecialChestDlg(owner: JFrame, title: String, val config: AppConfig, val noitaData: NoitaData) : OkCancelModalDialog<ChestConfig>(owner, title) {
	private val tabPane = JTabbedPane()

	private val entPanel = JPanel(BorderLayout())
	private val entSearchTextField = JTextField()
	private val entTableModel = EntityTableModel()
	private val entTable = JTable(entTableModel)
	private val entScrollPane = JScrollPane(entTable)

	private val spellPanel = JPanel(BorderLayout())
	private val spellSearchTextField = JTextField()
	private val spellTableModel = SpellTableModel()
	private val spellTable = JTable(spellTableModel)
	private val spellScrollPane = JScrollPane(spellTable)

	private val perkPanel = JPanel(BorderLayout())
	private val perkSearchTextField = JTextField()
	private val perkTableModel = PerkTableModel()
	private val perkTable = JTable(perkTableModel)
	private val perkScrollPane = JScrollPane(perkTable)

	init {
		entTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		entTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		spellTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		spellTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION

		perkTable.setDefaultRenderer(BufferedImage::class.java, EntityTableImgCellRenderer(32))
		perkTable.selectionModel.selectionMode = ListSelectionModel.SINGLE_SELECTION
	}

	override fun buildUi(): JPanel {
		val mainPanel = PaddedPanel()
		mainPanel.layout = BorderLayout()

		val entSearchPanel = PaddedPanel()
		entSearchPanel.layout = BoxLayout(entSearchPanel, BoxLayout.X_AXIS)
		entSearchPanel.add(JLabel("search:"))
		entSearchPanel.add(entSearchTextField)
		entPanel.add(entSearchPanel, BorderLayout.NORTH)
		entPanel.add(entScrollPane, BorderLayout.CENTER)

		val spellSearchPanel = PaddedPanel()
		spellSearchPanel.layout = BoxLayout(spellSearchPanel, BoxLayout.X_AXIS)
		spellSearchPanel.add(JLabel("search:"))
		spellSearchPanel.add(spellSearchTextField)
		spellPanel.add(spellSearchPanel, BorderLayout.NORTH)
		spellPanel.add(spellScrollPane, BorderLayout.CENTER)

		val perkSearchPanel = PaddedPanel()
		perkSearchPanel.layout = BoxLayout(perkSearchPanel, BoxLayout.X_AXIS)
		perkSearchPanel.add(JLabel("search:"))
		perkSearchPanel.add(perkSearchTextField)
		perkPanel.add(perkSearchPanel, BorderLayout.NORTH)
		perkPanel.add(perkScrollPane, BorderLayout.CENTER)

		entTableModel.setEntities(noitaData.entities)
		entTable.rowHeight = 32
		perkTableModel.setPerks(noitaData.perks)
		perkTable.rowHeight = 32
		spellTableModel.setSpells(noitaData.spells)
		spellTable.rowHeight = 32

		tabPane.addTab("entities", entPanel)
		tabPane.addTab("spells", spellPanel)
		tabPane.addTab("perks", perkPanel)

		entSearchTextField.addActionListener { onEntSearchText(it) }
		spellSearchTextField.addActionListener { onSpellSearchText(it) }
		perkSearchTextField.addActionListener { onPerkSearchText(it) }

		mainPanel.add(tabPane, BorderLayout.CENTER)

		return mainPanel
	}

	override fun getResult(): ChestConfig {
		println("tab pane ${tabPane.selectedIndex}")
		return when (tabPane.selectedIndex) {
			0 -> {
				entTableModel.entityAt(entTable.selectedRow)?.let { entity ->
					ChestConfig(SceneObjType.ENTITY, entity.entity_filename)
				}
			}
			1 -> {
				spellTableModel.spellAt(spellTable.selectedRow)?.let { spell ->
					ChestConfig(SceneObjType.SPELL, spell.id)
				}
			}
			2 -> {
				perkTableModel.perkAt(perkTable.selectedRow)?.let { perk ->
					ChestConfig(SceneObjType.PERK, perk.id)
				}
			}
			else -> {
				throw RuntimeException("tried to select a non-existent tab index")
			}
		} ?: throw RuntimeException("select a thing to be in the chest")
	}


	private fun onEntSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			entTableModel.setEntities(noitaData.entities)
		} else {
			entTableModel.setEntities(noitaData.entities.filter {
				it.short_filename.contains(txt, true) ||
					it.name.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onSpellSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			spellTableModel.setSpells(noitaData.spells)
		} else {
			spellTableModel.setSpells(noitaData.spells.filter {
				it.name.contains(txt, true) ||
					it.description.contains(txt, true) ||
					it.english_name.contains(txt, true)
			})
		}
	}

	private fun onPerkSearchText(e: ActionEvent) {
		val txt = e.actionCommand.trim()
		if (txt.isEmpty()) {
			perkTableModel.setPerks(noitaData.perks)
		} else {
			perkTableModel.setPerks(noitaData.perks.filter {
				it.ui_name.contains(txt, true) ||
					it.ui_description.contains(txt, true) ||
					it.english_name.contains(txt, true) ||
					it.english_desc.contains(txt, true)
			})
		}
	}
}