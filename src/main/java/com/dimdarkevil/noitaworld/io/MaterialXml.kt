package com.dimdarkevil.noitaworld.io

import com.dimdarkevil.noitaworld.HOME
import com.dimdarkevil.noitaworld.model.Material
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.parser.Parser
import java.io.File

object MaterialXml {
	val saveFolder = File(HOME, "AppData/LocalLow/Nolla_Games_Noita")

	@JvmStatic
	fun main(args: Array<String>) {
		val file = File(saveFolder, "data/materials.xml")
		val mats = loadMaterials(file)
		mats.forEach { println("$it") }
	}

	fun loadMaterials(xmlFile: File) : List<Material> {
		val doc = Jsoup.parse(xmlFile, "UTF-8", "", Parser.xmlParser())
		val cellData = doc.select("CellData").map { el ->
			materialFromElement(el)
		}
		val cellDataChild = doc.select("CellDataChild").map { el ->
			materialFromElement(el)
		}
		return (cellData + cellDataChild)
	}

	private fun materialFromElement(el: Element) = Material(
		name = el.attributes()["name"],
		ui_name = el.attributes()["ui_name"],
		_inherit_reactions = el.attributes()["_inherit_reactions"],
		_parent = el.attributes()["_parent"],
		burnable = el.attributes()["burnable"],
		cell_type = el.attributes()["cell_type"],
		collapsible = el.attributes()["collapsible"],
		color = el.attributes()["color"],
		crackability = el.attributes()["crackability"],
		density = el.attributes()["density"],
		durability = el.attributes()["durability"],
		electrical_conductivity = el.attributes()["electrical_conductivity"],
		explosion_power = el.attributes()["explosion_power"],
		generates_flames = el.attributes()["generates_flames"],
		generates_smoke = el.attributes()["generates_smoke"],
		hp = el.attributes()["hp"],
		is_just_particle_fx = el.attributes()["is_just_particle_fx"],
		lifetime = el.attributes()["lifetime"],
		liquid_damping = el.attributes()["liquid_damping"],
		liquid_flow_speed = el.attributes()["liquid_flow_speed"],
		liquid_gravity = el.attributes()["liquid_gravity"],
		liquid_sand = el.attributes()["liquid_sand"],
		liquid_slime = el.attributes()["liquid_slime"],
		liquid_solid = el.attributes()["liquid_solid"],
		liquid_static = el.attributes()["liquid_static"],
		liquid_viscosity = el.attributes()["liquid_viscosity"],
		solid_friction = el.attributes()["solid_friction"],
		solid_gravity_scale = el.attributes()["solid_gravity_scale"],
		solid_restitution = el.attributes()["solid_restitution"],
		solid_static_type = el.attributes()["solid_static_type"],
		stickyness = el.attributes()["stickyness"],
		wang_color = el.attributes()["wang_color"],
		tags = el.attributes()["tags"].split(",").map { it.trim() }.toSet(),
	)
}