package com.dimdarkevil.noitaworld.io

import com.dimdarkevil.noitaworld.HOME
import com.dimdarkevil.noitaworld.model.Biome
import org.xml.sax.Attributes
import org.xml.sax.SAXParseException
import org.xml.sax.helpers.DefaultHandler
import java.io.File
import java.io.FileInputStream
import javax.xml.parsers.SAXParserFactory

object BiomesAllXml {

	@JvmStatic
	fun main(args: Array<String>) {
		val file = File(HOME, "AppData/LocalLow/Nolla_Games_Noita/data/biome/_biomes_all.xml")
		val biomes = loadBiomesAll(file)
		biomes.forEach { println("$it") }
	}

	fun loadBiomesAll(xmlFile: File) : List<Biome> {
		return SAXParserFactory.newInstance().newSAXParser().let { parser ->
			FileInputStream(xmlFile).use { fis ->
				BiomesAllXmlHandler().let { handler ->
					parser.parse(fis, handler)
					handler.biomes
				}
			}
		}
	}

	class BiomesAllXmlHandler : DefaultHandler() {
		val biomes = mutableListOf<Biome>()

		override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
			if (qName != "Biome") return
			biomes.add(Biome(
				biome_filename = attributes.getValue("biome_filename"),
				height_index = attributes.getValue("height_index").toInt(),
				color = attributes.getValue("color"),
			))
		}

		override fun warning(e: SAXParseException) {
			throw e
		}

		override fun error(e: SAXParseException) {
			throw e
		}

		override fun fatalError(e: SAXParseException) {
			throw e
		}
	}
}

